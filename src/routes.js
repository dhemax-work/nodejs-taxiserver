'use strict';

const Structures = require('./structures');
const Matchmaking = require('./matchmaking');
const axios = require('axios').default;
const CustomStrings = require('./classes/strings/strings');


const SocketUsers = [];
class SocketsUser {
    constructor() {
        this.ClientID = null;
        this.SocketID = null;
        this.SocketIP = null;
    }
}


class Routes {
    constructor(socket) {
        this.io = socket;
        this.driver_space = this.io.of('/driver');
        this.client_space = this.io.of('/client');
        this.driver_socket = null;
        this.client_socket = null;

        this.clientid = null;
    }

    async loopEmit() {
        while (true) {
            try {
                await this.delay(2500);
                //TODO Change ClientList to InRouteClients List
                if (Structures.ClientList.length > 0) {
                    for (let i = 0; i <= Structures.ClientList.length; i++) {
                        if (typeof (Structures.ClientList[i].ClientID) != 'undefined') {
                            let cID = Structures.ClientList[i].ClientID.toString();
                            this.emitForMatch(cID);
                        }
                    }
                }
            } catch (Ex) {
                //console.log("A error on loop for Emit Travel: ", Ex);
                this.loopEmit();
                break;
            }
        }
    }

    emitForMatch(ClientID, Event = null, Data = null) {
        let Travel = new Matchmaking.Matchmaking().getTravel(ClientID);
        if (Travel !== null) {
            if (Event == null) {
                Event = "getMyTravel";
            }

            if (Data == null) {
                Data = Travel;
            }

            this.client_socket.in('clientRoom-' + Travel.TheClient.SocketID).emit(Event, Data);
            this.driver_socket.in('driverRoom-' + Travel.Vehicle.SocketID).emit(Event, Data);

            console.log("Emmit for travel", Travel.TravelID, "[e: ", Event, "] [clients: ", Travel.TheClient.ClientID, Travel.Vehicle.VehicleID, "]");
        }
    }

    async listenUnions() {
        while (true) {
            try {
                await this.delay(4000);
                for (let i = 0; i <= Structures.MatchesList.length; i++) {
                    if (Structures.MatchesList.length < 1) {
                        break;
                    }
                    if (Structures.MatchesList[i].TravelID != null || Structures.MatchesList[i].TravelID != undefined) {
                        if (Structures.MatchesList[i].SameRoom !== true) {
                            await this.delay(2000);
                            if (Structures.MatchesList[i].Vehicle.VehicleID != null || Structures.MatchesList[i].Vehicle.VehicleID !== "") {
                                await this.delay(200);
                                let Travel = Structures.MatchesList[i];
                                Travel.SameRoom = true;
                                await this.delay(200);

                                this.emitForMatch(Travel.TheClient.ClientID);
                                console.log("Client-Driver connected to same channel. [", Travel.TheClient.ClientID, Travel.Vehicle.VehicleID, "].");
                            }
                        }
                        break;
                    }
                }
            } catch (Exception) {
                console.log("Error, restarting listening unions.", Exception);
                this.listenUnions();
                break;
            }
        }
    }

    async cancelMatch(Travel, isDriver = false, isFinish = false) {
        console.log("A match has been canceled, ", Travel);
        let MyService = new Structures.FindInStructure(Structures.VehicleList, Travel.Vehicle.VehicleID);

        if (isFinish) {
            await this.delay(200);
            console.log("Travel ", Travel.TravelID, "has been finished");
            this.emitForMatch(Travel.TheClient.ClientID, 'travelFinished', Travel);
        } else {
            if (isDriver) {
                console.log("Travel ", Travel.TravelID, "has been canceled by driver");
                this.emitForMatch(Travel.TheClient.ClientID, 'canceledTravelByDriver', Travel);
            } else {
                console.log("Travel ", Travel.TravelID, "has been canceled by client");
                this.emitForMatch(Travel.TheClient.ClientID, 'canceledTravelByClient', Travel);
            }
        }

        if (MyService.ContainsVehicleID()) {
            MyService.Get().Status = 0;
        }

        await this.delay(300);
        new Matchmaking.Matchmaking().removeClientInRoute(Travel.TheClient.ClientID);

        console.log("Broken Travel: ", Travel.TravelID);
        new Matchmaking.Matchmaking().removeMatch(Travel.TravelID);
    }

    async socketEvents() {
        let ClientList = Structures.ClientList;
        let VehicleList = Structures.VehicleList;

        //Namespaces
        const admin_space = this.io.of('/admin');
        //const main_space = this.io;

        admin_space.on('connection', socket => {
            setInterval(function () {
                socket.emit("heartbeat", "I'm a Admin Alive!");

                let FakePosition = new Structures.GPSLocation();
                FakePosition.XPosition = "-33.013229";
                FakePosition.ZPosition = "-71.550085";

                let NearCars = new Matchmaking.Matchmaking().getLocalVehicles(FakePosition, 2);
                console.log(NearCars);
            }, 5000);

            console.log('Admin connection ' + socket.client.id + " ip " + socket.handshake.address);

            socket.on('getClients', () => {
                //console.log("Request From:", socket.client.id, "Get Clients: ", ClientList);
                socket.emit("getClients", ClientList);
            });

            socket.on('getVehicles', () => {
                //console.log("Request From:", socket.id, "Get Vehicles: ", VehicleList);
                socket.emit('getVehicles', VehicleList);
            });

            socket.on('getSocketClients', () => {
                //console.log("We have a ", SocketUsers.length, "socket users online");
                socket.emit('getSocketClients', SocketUsers);
            });

            socket.on('getWhiteListClients', () => {
                //console.log("We have a ", Matchmaking.WhiteList, "whitelist users");
                console.log(Matchmaking.WhiteList);
            });

            socket.on('getAllMatch', () => {
                //console.log("We have a ", Structures.MatchesList.length, "current matches users");
                console.log(Structures.MatchesList);
            });
        });

        await this.client_space.on('connection', socket => {
            this.client_socket = socket;

            let roomName = 'clientRoom_' + socket.client.id;
            socket.join(roomName);

            setInterval(function () {
                socket.emit("heartbeat", { message: 'I´m a Client Live!', room: socket.rooms, socketID: socket.client.id });

                let GetMyClient = new Structures.FindInStructure(Structures.ClientList, socket.client.id);
                if (GetMyClient.ContainsSocketID()) {
                    GetMyClient = GetMyClient.Get();
                    let calcDate = 60 * 50;
                    if ((new Date().getTime() - GetMyClient.UpdateStamp) >= calcDate) {
                        let Travel = new Matchmaking.Matchmaking().getTravel(GetMyClient.ClientID);
                        if (Travel !== null) {
                            console.log("A inactive travel has been removed.", Travel);
                            self.cancelMatch(Travel, false, true);
                        }
                        console.log('A inactive travel has been removed.', GetMyClient);
                        socket.disconnect();
                    }
                }
            }, 5000, this);


            socket.on('newClient', (data) => {
                try {
                    if (data.ClientID === "" || data.ClientID === null) {
                        console.log("A client socket", socket.id, "send a incorrect data. [ClientID empty]");
                        return;
                    }

                    let AClient = new Structures.FindInStructure(Structures.ClientList, data.ClientID);
                    let NewClientPosition = new Structures.GPSLocation();
                    NewClientPosition.XPosition = data.ClientPosition.XPosition;
                    NewClientPosition.ZPosition = data.ClientPosition.ZPosition;

                    if (!AClient.ContainsClientID()) {
                        let NewClient = new Structures.PassengerClient();

                        NewClient.SocketID = socket.client.id;
                        NewClient.ClientID = data.ClientID;
                        NewClient.ClientIP = socket.handshake.address;
                        NewClient.ClientPosition = NewClientPosition;
                        NewClient.ClientDestination = new Structures.GPSLocation;

                        Structures.ClientList.push(NewClient);
                        let vehicles = new Matchmaking.Matchmaking().getLocalVehicles(NewClient.ClientPosition);
                        this.client_space.in(roomName).emit('getAvailableVehicles', vehicles);
                        this.clientid = data.ClientID;
                        console.log("A new client has been added: ", NewClient, "find", vehicles.length, "vehicles near.");
                    } else {
                        let Client = AClient.Get();
                        if (Client.SocketID !== socket.client.id) {
                            Client.SocketID = socket.client.id;
                            console.log("Update Socket Client", Client.SocketID, "to", socket.client.id);
                        }

                        Client.ClientPosition = NewClientPosition;
                        this.client_space.in(roomName).emit('getMyClient', AClient.Get());
                    }
                } catch (exception) {
                    this.delay(1000);
                    console.log("Client registration failed for socket ", socket.id, "data:", data, "error:", exception);
                }
            });

            socket.on('getMyClient', () => {
                let TheList = new Structures.FindInStructure(Structures.ClientList, socket.client.id);
                if (TheList.ContainsSocketID()) {
                    let ClientData = TheList.Get();
                    console.log("Request Client Data: ", ClientData);
                    this.client_space.in(roomName).emit('getMyClient', ClientData);
                } else {
                    this.client_space.in(roomName).emit('getMyClient', null);
                }
            });

            socket.on('newTravelConfirm', (data) => {
                try {
                    let Client = new Structures.FindInStructure(Structures.ClientList, socket.client.id);

                    let IsClientInWhiteList = new Structures.FindInStructure(Matchmaking.WhiteList, Client.Get().ClientID);
                    let InRoute = new Structures.FindInStructure(Structures.ClientsInRoute, Client.Get().ClientID).ContainsClientID();
                    let MMaking = new Matchmaking.Matchmaking();

                    let TravelID = CustomStrings.getRandomString(7, false);

                    if (Client.Get().ClientID == null || Client.Get().ClientID === "") {
                        this.client_space.in(roomName).emit('getMyClient', null);
                    } else {
                        //console.log("Client WL:",IsClientInWhiteList.Contains(), "InRoute:", InRoute);
                        if (!IsClientInWhiteList.Contains() && !InRoute) {
                            let Destination = new Structures.GPSLocation();
                            Destination.ZPosition = data.ZPosition;
                            Destination.XPosition = data.XPosition;

                            Client.Get().ClientDestination = Destination;
                            console.log("A ClientID", Client.Get().ClientID, "update destination to", Destination);

                            Matchmaking.WhiteList.push(Client.Get().ClientID);
                            this.delay(300);



                            let MyTravel = MMaking.createNewTravel(Client.Get(), TravelID);
                            this.delay(400);
                            console.log("New travel of: ", socket.client.id, " confirmed to: ", Client.Get().ClientDestination, " adding to whitelist.");
                            this.client_space.in(roomName).emit('waitingVehicle', MyTravel, Structures.VehicleList.length);
                        }
                    }
                } catch (exception) {
                    console.log("Travel has been send without information, this omitted.", exception);
                }
            });

            socket.on('canceledTravelByClient', (ClientID) => {
                let Travel = new Matchmaking.Matchmaking().getTravel(ClientID);
                if (Travel !== null) {
                    console.log("Canceling travel by client ", Travel.TheClient.ClientID);
                    this.cancelMatch(Travel);
                }
            });

            socket.on('getMyTravel', (ClientID) => {
                let Travel = new Matchmaking.Matchmaking().getTravel(ClientID);
                //console.log("Travel Request:", Travel);
                socket.emit('getMyTravel', Travel);
            });

            console.log('Client connection ' + socket.client.id + " ip " + socket.handshake.address);
            socket.on('disconnecting', (reason) => {
                console.log("A client", socket.id, "has been disconnected by", reason);
            });
        });

        await this.driver_space.on('connection', socket => {
            this.driver_socket = socket;
            let roomName = 'driverRoom_' + socket.client.id;
            console.log('Driver connection ' + socket.client.id + " ip " + socket.handshake.address);


            setInterval(function () {
                socket.emit("heartbeat", "I'm Driver Alive!");

                let MyService = new Structures.FindInStructure(Structures.VehicleList, socket.client.id);
                if (MyService.ContainsSocketID()) {
                    socket.emit('getMyVehicle', MyService.Get());
                }
            }, 4500);

            socket.on('updateTravel', data => {
                let Travel = new Matchmaking.Matchmaking().getTravel(data.VehicleID, true);
                let MyService = new Structures.FindInStructure(Structures.VehicleList, data.VehicleID);

                if (MyService.ContainsVehicleID()) {
                    if (Travel !== null) {
                        let NewMarker = new Structures.MarkersMap();
                        let NewStat = new Structures.Stats();

                        NewStat.Speed = data.Markers.Stats.Speed;
                        NewStat.Odometer = data.Markers.Stats.Odometer;
                        NewStat.Price = data.Markers.Stats.Price;

                        NewMarker.Stamp = data.Markers.Timestamp;
                        NewMarker.Stats = NewStat;

                        Travel.Vehicle = MyService.Get();
                        Travel.Markers = NewMarker;
                        console.log("Travel ", Travel.TravelID, "has been updated");
                    }
                }
            });

            socket.on('newService', (data) => {
                let AVehicle = new Structures.FindInStructure(Structures.VehicleList, data.VehicleID);
                let NewLocation = new Structures.GPSLocation();

                NewLocation.XPosition = data.Location.XPosition;
                NewLocation.ZPosition = data.Location.ZPosition;

                if (!AVehicle.ContainsVehicleID()) {
                    let NewVehicle = new Structures.Vehicle();

                    NewVehicle.SocketID = socket.client.id;
                    NewVehicle.VehicleID = data.VehicleID;
                    NewVehicle.Location = NewLocation;
                    NewVehicle.rfID = data.rfID;
                    NewVehicle.Status = 0;

                    Structures.VehicleList.push(NewVehicle);
                    this.driver_space.in(roomName).emit('getLocalClients', 'for now, a simple message');
                    console.log("A new vehicle start his service: ", NewVehicle);
                } else {
                    let Vehicle = AVehicle.Get();
                    Vehicle.SocketID = socket.client.id;
                    Vehicle.Location = NewLocation;

                    this.driver_space.in(roomName).emit('getMyVehicle', AVehicle.Get());
                }
            });

            socket.on('getMyTravel', (DriverID) => {
                let Travel = new Matchmaking.Matchmaking().getTravel(DriverID, true);
                socket.emit('getMyTravel', Travel);
            });

            socket.on('getMyVehicle', () => {
                console.log("A driver ", socket.client.id, "request a getMyVehicle");
                let MyService = new Structures.FindInStructure(Structures.VehicleList, socket.client.id);
                let response;

                if (MyService.ContainsSocketID()) {
                    response = MyService.Get();
                } else {
                    response = new Structures.Vehicle();
                }
                socket.emit('getMyVehicle', response);
            });

            socket.on('canceledTravelByDriver', data => {
                let DriverID = data.DriverID;
                let isFinish = data.TravelStatus;
                let Travel = new Matchmaking.Matchmaking().getTravel(DriverID, true);
                if (Travel !== null) {
                    if (isFinish) {
                        Travel.Status = 3;
                        console.log("Travel finish, waiting payment.");
                        this.cancelMatch(Travel, null, true);
                    } else {
                        console.log("Canceling travel by driver ", Travel.Vehicle.VehicleID);
                        this.cancelMatch(Travel, true);
                    }
                }
            });

            socket.on('imWithClient', (client) => {
                let Travel = new Matchmaking.Matchmaking().getTravel(client);
                if (Travel != null) {
                    Travel.Status = 2;
                    console.log("Travel", Travel.TravelID, "update, customer approaches the car ", Travel.Vehicle.VehicleID);
                }
            });

            socket.on('disconnecting', (reason) => {
                console.log("A driver", socket.id, "has been disconnected by", reason);
            });
        });

        this.io.on('connection', socket => {
            setInterval(function () {
                socket.emit("heartbeat", {
                    message: 'I´m A Live!',
                    room: socket.rooms,
                    socketID: socket.client.id
                });
            }, 5000);

            let newSocket = new SocketsUser();
            newSocket.SocketID = socket.client.id;
            newSocket.SocketIP = socket.handshake.address;

            SocketUsers.push(newSocket);
            console.log('New connection ' + socket.client.id + " ip " + socket.handshake.address);
        });

        await this.io.on('disconnect', socket => {

            let aClient = new Structures.FindInStructure(null, socket.id);
            if (aClient.ContainsSocketID()) {
                aClient.ClearClient();
            }

            for (let i = 0; i < SocketsUser.length; i++) {
                if (SocketsUser[i].SocketID === socket.client.id) {
                    SocketsUser.splice(i, 1);
                }
            }
            this.io.emit('exit', SocketUsers);
            console.log("Disconnection " + socket.client.id);
        });

        this.listenUnions();
        this.loopEmit();
    }

    async routesConfig() {
        await this.socketEvents();
    }

    async delay(mills) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(mills);
            }, mills);
        });
    }
}
module.exports = {
    Routes
};