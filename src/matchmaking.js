let WhiteList = [];
const Structures = require('./structures');
const geolib = require('geolib');



//const Routes = require('./routes');

class Matchmaking {
    constructor() { }

    // isDriverOnRoute(driverID){
    //     for (let i=0; i < Structures.MatchesList.length; i++){
    //         if(Structures.MatchesList[i].Vehicle.VehicleID === driverID){
    //             return true;
    //         }
    //     }
    //     return false;
    // }

    removeMatch(MatchID) {
        for (let i = 0; i < Structures.MatchesList.length; i++) {
            if (Structures.MatchesList[i].TravelID === MatchID) {
                Structures.MatchesList.splice(i, 1);
            }
        }
    }

    // addClientToWhitelist(ClientID){
    //     for (let i=0; i <= WhiteList.length; WhiteList++){
    //         if(WhiteList[i] === ClientID){
    //             return false;
    //         }else{
    //             WhiteList.push(ClientID);
    //         }
    //     }
    // }

    removeClientInWhiteList(clientID) {
        for (let i = 0; i < WhiteList.length; i++) {
            if (WhiteList[i] === clientID) {
                WhiteList.splice(i, 1);
            }
        }
    }

    removeClientInRoute(clientID) {
        for (let i = 0; i < Structures.ClientsInRoute.length; i++) {
            if (Structures.ClientsInRoute[i] === clientID) {
                Structures.ClientsInRoute.splice(i, 1);
            }
        }
    }

    // removeMach(machID){
    //     for (let i = 0; i < Structures.MatchesList.length; i++){
    //         if(Structures.MatchesList[i].ClientID === machID){
    //             Structures.MatchesList.splice(i, 1);
    //         }
    //     }
    // }

    createNewTravel(client, travelID) {
        let Travel = new Structures.Matches();
        Travel.TravelID = travelID;
        Travel.TheClient = client;
        Travel.StartLocation = client.ClientPosition;
        Travel.EndLocation = client.ClientDestination;
        Travel.Markers = new Structures.MarkersMap();
        Travel.Status = 0;

        console.log("A new travel has been confirmed: ", Travel.TravelID, " waiting for driver.");
        Structures.MatchesList.push(Travel);
        //Structures.ClientsInRoute.push(client);

        return Travel;
    }

    makeId(length) {
        let result = "";
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;

        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    waitTime(mills) {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(mills);
            }, mills);
        });
    }

    calculateVehiclesDistance(ClientPosition, VehiclePosition) {
        let xClient = parseFloat(ClientPosition.XPosition);
        let zClient = parseFloat(ClientPosition.ZPosition);

        console.log("ClIENT", xClient, zClient);

        let xDriver = parseFloat(VehiclePosition.XPosition);
        let zDriver = parseFloat(VehiclePosition.ZPosition);

        console.log("DRIVER", xDriver, zDriver);

        return geolib.getDistance(
            { latitude: xDriver, longitude: zDriver },
            { latitude: xClient, longitude: zClient }
        );
    }

    getLocalVehicles(ClientPosition, distance) {
        this._distance = distance;
        //console.log("DISTANCE:", distance);

        let localVehicles = [];

        for (let i = 0; i < Structures.VehicleList.length; i++) {
            if (i === 9) return localVehicles;
            this._distance = this.calculateVehiclesDistance(ClientPosition, Structures.VehicleList[i].Location);

            console.log("Ahhh", this._distance);
            let TheVehicle = new Structures.FindInStructure(Structures.VehicleList, Structures.VehicleList[i].VehicleID);
            if (TheVehicle.ContainsVehicleID()) {
                if (distance < Math.round(this._distance) * 1000) {
                    localVehicles.push(Structures.VehicleList[i]);
                }
            }
        }
        return localVehicles;
    }

    // async waitingForAcceptOrReject(Travel){
    //     let driver_socket = new Routes(null).driver_space;
    //     let roomName = 'driverRoom_'+Travel.Vehicle.SocketID;
    //     driver_socket.in(roomName).emit('acceptOrRejectTravel', Travel);
    //     let timeout = 10;
    //     for ( ; timeout >= 0; timeout --){
    //         driver_socket.on('acceptTravel', () => {
    //             return true;
    //         });
    //
    //         driver_socket.on('rejectTravel', () => {
    //             return false;
    //         });
    //
    //         await this.waitTime(1000);
    //     }
    //     if(timeout === 0){
    //         return false;
    //     }
    // }

    getTravel(ClientID, ForDriver = false) {
        let response = null;
        try {
            if (ClientID === null) {
                this.waitTime(1000);
                console.log("A given ID is NULL (getTravel)");
                return;
            }

            if (Structures.MatchesList.length > 0) {
                for (let i = 0; i <= Structures.MatchesList.length; i++) {
                    if (ForDriver) {
                        if (Structures.MatchesList[i].Vehicle.VehicleID === ClientID) {
                            return Structures.MatchesList[i];
                        }
                    } else {
                        if (Structures.MatchesList[i].TheClient.ClientID === ClientID) {
                            return Structures.MatchesList[i];
                        }
                    }
                }
            }
        } catch (e) {
            console.log(e);
        }
        return response;
    }


    // async searchDriver(Travel){
    //     //let LocalVehicles = new Matchmaking.Matchmaking().getLocalVehicles(Travel.Client.ClientPosition);
    //     let LocalVehicles = Structures.VehicleList;
    //
    //     for (let vehicle=0; vehicle < LocalVehicles.length; vehicle++){
    //         let RealVehicle = new Structures.FindInStructure(Structures.VehicleList, LocalVehicles[vehicle].VehicleID);
    //         if(RealVehicle.ContainsVehicleID()){
    //             RealVehicle = RealVehicle.Get();
    //             if(RealVehicle.Status === 0){
    //                 console.log("Travel Found with a possible driver: ", RealVehicle);
    //                 await this.waitTime(300);
    //
    //                 if((Travel.TheClient.ClientID != null) || (Travel.TheClient.ClientID !== "")){
    //                     RealVehicle.Status = 1;
    //                     Travel.Vehicle = RealVehicle;
    //
    //                     Travel.Status = 1;
    //
    //                     await this.waitTime(100);
    //                     this.removeClientInWhiteList(Travel.TheClient.ClientID);
    //                     console.log("MATCH: Vehicle ", Travel.Vehicle, " with client ", Travel.TheClient, "[Waiting for accept]");
    //
    //                     if(!this.waitingForAcceptOrReject(Travel)){
    //                         this.searchDriver(Travel);
    //                         break;
    //                     }else{
    //                         return true;
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }

    async runService() {
        console.log("Starting MatchMaking");
        await this.waitTime(4000);
        while (true) {
            try {
                await this.waitTime(500);
                //console.log("We have a ", Routes.SocketUsers.length, "connected users in socket.");
                if (Structures.VehicleList.length < 1) {
                    console.log("[" + new Date() + "] No available vehicles for depart");
                    await this.waitTime(2500);
                } else {
                    if (WhiteList.length > 0) {
                        for (let windex = 0; windex < WhiteList.length; windex++) {
                            if (WhiteList[windex] != null || WhiteList[windex] !== "") {
                                let Travel = this.getTravel(WhiteList[windex]);
                                //console.log(Travel, "TheClient?", WhiteList[windex]);

                                if (Travel !== false || Travel.Status !== 0) {
                                    console.log("Found a new travel: ", Travel);
                                    let LocalVehicles = this.getLocalVehicles(Travel.TheClient.ClientPosition, 2);
                                    //let LocalVehicles = Structures.VehicleList;

                                    //console.log(LocalVehicles);

                                    if (LocalVehicles.length <= 0) {
                                        await this.waitTime(1500);
                                        console.log("A client searching by a car... but nothing near their, expanding search...");
                                        LocalVehicles = this.getLocalVehicles(Travel.TheClient.ClientPosition, 3);
                                    } else {
                                        for (let vehicle = 0; vehicle < LocalVehicles.length; vehicle++) {
                                            if (LocalVehicles[vehicle].Status === 0) {
                                                let RealVehicle = new Structures.FindInStructure(Structures.VehicleList, LocalVehicles[vehicle].VehicleID);
                                                if ((Travel.TheClient.ClientID != null) || (Travel.TheClient.ClientID !== "")) {
                                                    RealVehicle.Get().Status = 1;

                                                    let StructureToSend = {
                                                        VehicleID: RealVehicle.Get().VehicleID,
                                                        AccountID: Travel.TheClient.ClientID,
                                                        StartLocation: Travel.TheClient.ClientPosition,
                                                        EndLocation: Travel.TheClient.ClientDestination,
                                                        Stats: {
                                                            Price: 0,
                                                            Odometer: 0
                                                        },
                                                        Status: 1,
                                                        Created: new Date().getTime(),
                                                        Updated: new Date().getTime(),
                                                    };
                                                    await this.waitTime(500);

                                                    Travel.Vehicle = RealVehicle.Get();

                                                    Travel.EndLocation = Travel.TheClient.ClientDestination;
                                                    Travel.Status = 1;
                                                    Travel.SameRoom = false;

                                                    //this.removeMach(Travel.TravelID);
                                                    //Structures.MatchesList.push(Travel);
                                                    console.log("Travel Driver Confirm:: ", Travel);


                                                    this.removeClientInWhiteList(Travel.TheClient.ClientID);

                                                    console.log("MATCH: Vehicle ", Travel.Vehicle, " with client ", Travel.TheClient);
                                                    // axios.post(process.env.ENDPOINT_URL + "/travels", StructureToSend).then(data => {

                                                    // }).catch(err => {
                                                    //     console.log("[RegisterMatch] A error has been appear!");
                                                    // });

                                                    this.runService();
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //TODO Ask for each driver for accept or reject travel

                            /*
                            let CanDrive = this.searchDriver(Travel);
                            if(!CanDrive){

                            }
                            */
                        }
                    }
                    // else{
                    //     await this.waitTime(10000);
                    //     console.log("No clients available, but we have [", Structures.VehicleList.length, "] vehicles and [", Structures.ClientList.length ,"] online users.");
                    // }
                }
            } catch (e) {
                console.log(e);
                this.runService();
                break;
            }
        }
    }
}


module.exports = {
    Matchmaking: Matchmaking,
    WhiteList: WhiteList
};