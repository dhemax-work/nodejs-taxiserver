
let ClientList = [];
let VehicleList = [];

let ClientsInRoute = [];
let MatchesList = [];

class GPSLocation {
    constructor() {
        this.XPosition = null;
        this.ZPosition = null;
    }
}

class PassengerClient {
    constructor() {
        this.ClientID = null;
        this.SocketID = null;
        this.ClientIP = null;
        this.ClientPosition = new GPSLocation();
        this.ClientDestination = new GPSLocation();
    }
}

class Matches {
    constructor() {
        this.TravelID = "";
        this.TheClient = new PassengerClient();
        this.Vehicle = new Vehicle();
        this.StartLocation = new GPSLocation();
        this.EndLocation = new GPSLocation();
        this.Markers = new MarkersMap();
        this.VehicleReject = [];
        this.SameRoom = false;
        this.Status = 0;
    }
}

class MarkersMap {
    constructor() {
        //this.Location = new GPSLocation();
        this.Stamp = new Date().getTime();
        this.Stats = new Stats();
    }

}

class Stats {
    constructor() {
        this.Speed = null;
        this.Odometer = null;
        this.Price = null;
    }
}

class Vehicle {
    constructor() {
        this.SocketID = null;
        this.VehicleID = null;
        this.Location = null;
        this.Status = null;
        this.UpdateStamp = new Date().getTime();
        this.rfID = null;
    }
}

class FindInStructure {
    /**
     * @return {boolean}
     */
    constructor(structure = null, value = null) {
        this.list = structure;
        this.toSearch = value;
        this.index = 0;
    }

    ContainsSocketID() {
        for (let i = 0; i < this.list.length; i++) {
            if (this.list[i].SocketID === this.toSearch) {
                this.index = i;
                return true;
            }
        }
        return false;
    }

    // ContainsIP(){
    //     for (let i = 0; i < this.list.length; i++){
    //         if(this.list[i].ClientIP === this.toSearch){
    //             this.index = i;
    //             return true;
    //         }
    //     }
    //     return false
    // }

    ContainsClientID() {
        for (let i = 0; i < this.list.length; i++) {
            if (this.list[i].ClientID === this.toSearch) {
                this.index = i;
                return true;
            }
        }
        return false;
    }

    Contains() {
        for (let i = 0; i < this.list.length; i++) {
            if (this.list[i] === this.toSearch) {
                this.index = i;
                return true;
            }
        }
        return false;
    }

    ContainsVehicleID() {
        for (let i = 0; i < this.list.length; i++) {
            if (this.list[i].VehicleID === this.toSearch) {
                this.index = i;
                return true;
            }
        }
        return false;
    }

    ContainsTravelID() {
        for (let i = 0; i < this.list.length; i++) {
            if (this.list[i].TravelID === this.toSearch) {
                this.index = i;
                return true;
            }
        }
        return false;
    }

    ClearClient() {
        for (let i = 0; i < ClientList.length; i++) {
            if (ClientList[i].SocketID === this.toSearch) {
                ClientList.splice(i, 1);
                console.log("A client destroyed!");
            }
        }
    }

    Get() {
        return this.list[this.index];
    }
}



module.exports = {
    ClientList: ClientList,
    VehicleList: VehicleList,
    ClientsInRoute: ClientsInRoute,
    MatchesList,
    GPSLocation: GPSLocation,
    Matches: Matches,
    MarkersMap: MarkersMap,
    Vehicle: Vehicle,
    PassengerClient: PassengerClient,
    Stats: Stats,
    FindInStructure: FindInStructure,
};

/*
* CLIENT OBJECT
*{
  "ClientID": 'ID_CLIENTE',
  "ClientIP": 'IP_CLIENTE',
  "ClientPosition": {
      "XPosition": 'MAPS_X',
      "ZPosition": 'MAPS_Z'
  },
  "ClientDestination": {
    "XPosition": 'MAPS_X_NULL',
      "ZPosition": 'MAPS_Z_NULL'
  }
}
*
* GPSLocation
* {
    "XPosition": "X MAP COORD",
    "ZPosition": "Z MAP COORD"
}
*
* NewTravelConfirmed
* {
  "ClientID": 'ID_CLIENTE',
  "ClientIP": 'IP_CLIENTE',
  "ClientPosition": {
      "XPosition": 'MAPS_X',
      "ZPosition": 'MAPS_Z'
  },
  "ClientDestination": {
    "XPosition": 'MAPS_X_NULL',
      "ZPosition": 'MAPS_Z_NULL'
  }
}
*
* */