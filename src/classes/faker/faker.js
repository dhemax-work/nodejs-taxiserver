const Structures = require('../../structures');

class CreateFake {
    constructor(count) { this.counter = count; }

    Travel() {
        let LocalGPS = new Structures.GPSLocation();
        LocalGPS.XPosition = "-33.021997" + (Math.floor(Math.random() * (10 * 5 - 5) + 5) / (2));
        LocalGPS.ZPosition = "-71.548701" + (Math.floor(Math.random() * (5 * 3 - 5) + 5) / (3));

        let i;
        for (i = 1; i <= this.counter; i++) {
            let NewVehicle = new Structures.Vehicle();
            NewVehicle.Location = LocalGPS;
            NewVehicle.SocketID = "abcde" + i;
            NewVehicle.Status = 0;
            NewVehicle.UpdateStamp = new Date().getDate();
            NewVehicle.rfID = "rfidtest" + i;

            let NewClient = new Structures.PassengerClient();
            NewClient.SocketID = "s-id" + i;
            NewClient.ClientID = "Client" + i;
            NewClient.ClientIP = "127.0.0." + i;
            NewClient.ClientPosition = LocalGPS;
            NewClient.ClientDestination = LocalGPS;

            let NewTravel = new Structures.Matches();

            NewTravel.Vehicle = NewVehicle;
            NewTravel.EndLocation = LocalGPS;
            NewTravel.StartLocation = LocalGPS;
            NewTravel.TravelID = "TestingTravel#" + i;
            NewTravel.Status = Math.random(3);
            NewTravel.TheClient = NewClient;


        }
        console.log(this.counter + " new fake travels.");
    }

    Vehicles() {
        let LocalGPS = new Structures.GPSLocation();
        LocalGPS.XPosition = "-33.021997" + (Math.floor(Math.random() * (10 * 5 - 5) + 5) / (2));
        LocalGPS.ZPosition = "-71.548701" + (Math.floor(Math.random() * (5 * 3 - 5) + 5) / (3));


        let i;
        for (i = 1; i <= this.counter; i++) {
            let NewVehicle = new Structures.Vehicle();
            NewVehicle.VehicleID = "Vehicle" + i;
            NewVehicle.Location = LocalGPS;
            NewVehicle.Status = Math.floor(Math.random() * 3) + 1;
            NewVehicle.rfID = "rfid" + i;

            Structures.VehicleList.push(NewVehicle);
        }
        console.log(this.counter + " new fake vehicles.");
    }

    Clients() {
        let LocalGPS = new Structures.GPSLocation();
        LocalGPS.XPosition = "147.15";
        LocalGPS.ZPosition = "0.58";

        let EndGPS = new Structures.GPSLocation();
        EndGPS.XPosition = "157.15";
        EndGPS.ZPosition = "1.58";

        let i;
        for (i = 1; i <= this.counter; i++) {
            let NewClient = new Structures.PassengerClient();
            NewClient.SocketID = "s-id" + i;
            NewClient.ClientID = "Client" + i;
            NewClient.ClientIP = "127.0.0." + i;
            NewClient.ClientPosition = LocalGPS;
            NewClient.ClientDestination = EndGPS;
            Structures.ClientList.push(NewClient);
        }
        console.log(this.counter + " new fake clients.");
    }
}

module.exports = {
    CreateFake: CreateFake
};