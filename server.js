
// Classes imports, this include all helpers for each module of server
const config = require('dotenv');
const Router = require('./src/routes');
const Matches = require('./src/matchmaking');
const Faker = require('./src/classes/faker/faker');
const express = require('express');
const server = require('http');
const socketio = require('socket.io');
const helmet = require('helmet');


// Config: DotEnv config
// Router: A SocketIO url services
// Matches: A Matchmaking system for cars/clients
// Faker: Custom library for fake socket clients / vehicles / Travels
// Express, Server: A web server
// Socketio, Helmet: SocketIO a main connection for server. Helmet a cors protection

let ConnectionsCount = 0;


express().use(helmet());
express().disable('X-powered-by');

config.config();


class Server {
  constructor() {
    this.port = process.env.MAIN_SOCKET_PORT;
    this.host = process.env.MAIN_SOCKET_IP;

    this.app = express();
    this.http = server.Server(this.app);
    this.socket = socketio(this.http);
  }

  includeRoute() {
    new Router.Routes(this.socket).routesConfig();
  }

  runMatchmaking() {
    new Matches.Matchmaking().runService().then(r => console.log("Matchmaking is running"));
  }

  createFake() {
    // new Faker.CreateFake(1).Clients();
    // new Faker.CreateFake(1).Vehicles();
    // new Faker.CreateFake(1).Travel();
  }

  checkExistingEnv() {
    if ("MAIN_SOCKET_PORT" in process.env) {
      return true;
    }
    return false;
  }

  delay(mills) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(mills);
      }, mills);
    });
  }


  appExecute() {

    if (!this.checkExistingEnv()) {
      return console.log("Env file not configured yet");
    }

    this.includeRoute();
    this.createFake();
    this.runMatchmaking();

    this.socket.set('heartbeat timeout', process.env.SOCKET_HEARTBEAT_TIMEOUT);
    this.socket.set('heartbeat interval', process.env.SOCKET_HEARTBEAT_INTERVAL);

    this.http.listen(this.port, this.host, () => {
      console.log(`Listening on http://${this.host}:${this.port}`);
    });
  }
}

const app = new Server();

app.appExecute();

